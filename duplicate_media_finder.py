#!/usr/bin/python
#-*- coding: iso-8859-1 -*-
# PYTHON_ARGCOMPLETE_OK
import argcomplete
import argparse

parser = argparse.ArgumentParser()
# parser.add_argument('--targets', '-t',           action='append',     help='File library location')
parser.add_argument('--single-threaded', '-s',   action='store_true', help='Run single threaded mode')
parser.add_argument('--output-file', '-o',       action='store',      help='Output file. Defaults to output.log')
parser.add_argument('--remove-duplicates', '-r', action='store_true', help='Remove duplicates')
parser.add_argument('--quiet', '-q',             action='store_true', help='Don\'t print progress')
parser.add_argument('targets', type=str, nargs='+')
argcomplete.autocomplete(parser)
args = parser.parse_args()

max_threads = 2
if args.single_threaded:
    max_threads = 1

if args.output_file is None:
    args.output_file = 'output.log'

if args.targets is None:
    raise RuntimeError("Must specify target directories")
# print(args)
# import sys
# print(sys.argv)
# sys.exit()

# End of the argcomplete section

from math import log
import os
import sys
import threading
import time

unit_list = zip(['bytes', 'kB', 'MB', 'GB', 'TB', 'PB'], [0, 0, 1, 2, 2, 2])

# photo_extensions = ['png', 'jpg', 'jpeg', 'bmp']
# video_extensions = ['mov', 'avi', 'mp4', '3gp', 'mpg', 'mpeg', 'wmv']
# audio_extensions = ['wav', 'amr']
# # other_extensions = ['ppsx', 'zip', '7z', '7zip', 'exe']
#
# media_extensions = photo_extensions + video_extensions + audio_extensions
# all_extensions = media_extensions# + other_extensions
all_extensions = ['*']

PID_FILE = '.pid.log'

def printf(s, *args):
    s = str(s)
    if len(args) != 0:
        sys.stdout.write(s % args)
    else:
        sys.stdout.write(s)
    sys.stdout.flush()

def time_fmt(s):
    m = int(s/60)
    s = s - 60*m
    return format("%d:%.2d" % (m, s))

def compare_files(a, b):
    if len(os.popen("diff \"" + a + "\" \"" + b + "\"", 'r').readlines()) == 0:
        return True
    return False

def humanize(num):
    """Human friendly file size"""
    if num > 1:
        exponent = min(int(log(num, 1024)), len(unit_list) - 1)
        quotient = float(num) / 1024**exponent
        unit, num_decimals = unit_list[exponent]
        format_string = '{:.%sf} {}' % (num_decimals)
        return format_string.format(quotient, unit)
    if num == 0:
        return '0 bytes'
    if num == 1:
        return '1 byte'

def build_size_to_file_list(directories):
    size_to_file = []
    temp_dict = {}
    for d in directories:
#         for l in os.popen("du -SbL --all \"" + d + "\" | egrep -i " + "\'\\." + "$|\\.".join(all_extensions) + "$\'", 'r'):
        for l in os.popen("du -SbL --all \"" + d + "\"", 'r'):
            ls = l[:-1].split("\t")
            size = int(ls[0])
            filename = "".join(ls[1:])
            try:
                os.path.getsize(filename)
            except:
                continue
            if os.path.isfile(filename):
                if temp_dict.get(size) is None:
                    temp_dict[size] = [filename]
                else:
                    temp_dict[size].append(filename)
                    temp_dict[size].sort()

    keys = temp_dict.keys()
    keys.sort()
    keys.reverse()


    # Interleave large and small so we process large files earlier but still have a smooth progress
    pick_large = True
    while True:
        if len(keys) == 0: break
        # pick_large = not pick_large
        if pick_large:
            k = keys.pop()
        else:
            k = keys[0]
            keys = keys[1:]

        size_to_file.append((k, temp_dict[k]))


#     import copy
#     t = copy.deepcopy(size_to_file)
#     for i in t:
#         print(humanize(i[0]))

    return size_to_file

def get_recursive_dir_list(directories):
    r = []
    for d in directories:
        for dirname, dirnames in os.walk(d)[0:2]:
            for d in dirnames:
                r.append(dirname + "/" + d)
    return r

class DuplicateFinder(object):
    def __init__(self, targets, max_threads = 10, output_file = None, remove_duplicates = False, quiet = False):
        fd = open(PID_FILE, 'w')
        fd.write(str(os.getpid()))
        fd.close()

        self.THREAD_LIMIT = max_threads

        self.targets = targets

        self.report_fd = open(output_file, 'w')
        self.remove_duplicates = remove_duplicates
        self.quiet = quiet

        self.checked_size = 0
        self.checked_cnt = 0
        self.dup_cnt = 0

        self.threads_created = 0
        self.threads_finished = 0
        self.max_threads = 0

        self.result_collector_lock = threading.Lock()

        # Flags when we should wait before creating more threads
        self.proceed = threading.Event()
        self.proceed.set()

        # Flags when all files have been processed
        self.done = threading.Event()

        if not self.quiet:
            # Prints the progress regularly
            self.STATUS_INTERVAL = 0.5
            self.print_status_t = threading.Timer(self.STATUS_INTERVAL, self.print_status)
            self.print_status_t.setName("print_status")

    def get_status(self):
        now = time.time()
        progress = 100.0*self.checked_size/self.total_size
        remaining_size = self.total_size - self.checked_size
        remaining_cnt = self.total_cnt - self.checked_cnt
        if self.checked_cnt != 0:
            avg_time_size = ((now - self.start_time)/self.checked_size)
            eta_size = avg_time_size*remaining_size

            avg_time_cnt = ((now - self.start_time)/self.checked_cnt)
            eta_cnt = avg_time_cnt*remaining_cnt

            eta = max(eta_size, eta_cnt)

            eta = time_fmt(eta)
        else:
            eta = 'n/a'

        s = "Progress: %.2f" % progress
        s += "%c" % 37
        s += ", scanned %d (%s) out of %d (%s) files | " % (self.checked_cnt, humanize(self.checked_size), self.total_cnt, humanize(self.total_size))
        s += "Number of duplicates: %d (%.2f" % (self.dup_cnt, 100.0*self.dup_cnt/self.total_cnt)
        s += "%c)" % 37
        s += " | active threads: %2d" % (self.threads_created - self.threads_finished)
        s += " (max %2d)" % self.max_threads
        s += " | ETA is " + eta
        columns = int(os.popen('stty size', 'r').read().split()[1])
        printf("\r" + s.ljust(columns))

    def print_status(self):
        self.get_status()
        if self.done.is_set(): return
        self.print_status_t = threading.Timer(self.STATUS_INTERVAL, self.print_status)
        self.print_status_t.setName("print_status")
        self.print_status_t.start()

    def result_collector(self, r):
        self.result_collector_lock.acquire()

        self.threads_finished += 1

        for f, match_list in r.iteritems():
            self.checked_size += os.path.getsize(f)
            self.checked_cnt += 1

            if len(match_list) != 0:
                self.dup_cnt += 1
                l = "%s," % str(f)
                l += ",".join(x for x in match_list)
                l += "\n"
                self.report_fd.write(l)

        self.result_collector_lock.release()

        if self.remove_duplicates:
            for f, match_list in r.iteritems():
                for x in match_list:
                    if os.path.isfile(x):
                        cmd = "rm -v \"" + x + "\""
                        assert(not os.system(cmd))

    def is_duplicate(self, f_list, threaded = True):
        if threaded:
            if self.threads_created - self.threads_finished >= self.THREAD_LIMIT and self.proceed.is_set():
                self.proceed.clear()

        matching = {}
        while len(f_list) != 0:
            f_ref = f_list.pop()
            matching[f_ref] = []
            for f_db in f_list:
                if compare_files(f_db, f_ref):
                    matching[f_ref].append(f_db)

        self.result_collector(matching)

        if threaded:
            if self.threads_created - self.threads_finished < self.THREAD_LIMIT and not self.proceed.is_set():
                self.proceed.set()
        if self.checked_cnt == self.total_cnt:
            self.done.set()

    def __call__(self):
        printf("Started with PID " + str(os.getpid()) +". Finding files... ")

        self.size_to_file = build_size_to_file_list(self.targets)

        self.total_size = 0
        self.total_cnt = 0

        for f in self.size_to_file:
            for i in f[1]:
                self.total_size += os.path.getsize(i)
                self.total_cnt += 1

        print("found %d files, a total of %s" % (self.total_cnt, humanize(self.total_size)))

        self.start_time = time.time()

        if not self.quiet:
            self.print_status_t.start()
        s_prev = ""
        try:
            while True:
                if self.THREAD_LIMIT != 1:
                    self.proceed.wait()

                if len(self.size_to_file) == 0: break

                self.result_collector_lock.acquire()
                f_size, f_list = self.size_to_file.pop()
                self.result_collector_lock.release()
                self.threads_created += 1
                if self.threads_created - self.threads_finished > self.max_threads:
                    self.max_threads = self.threads_created - self.threads_finished

                if self.THREAD_LIMIT == 1 or f_size < 15*1024*1024:
                    self.is_duplicate(f_list, threaded = False)
                elif len(f_list) == 1:
                    self.result_collector_lock.acquire()
                    self.checked_size += f_size
                    self.checked_cnt += 1
                    self.threads_finished += 1
                    self.result_collector_lock.release()
                else:
                    threading.Thread(target = self.is_duplicate, args = [f_list], name = 'checker').start()
                    # multiprocessing.Process(target = self.is_duplicate, args = [f_list], name = 'checker').start()
            self.proceed.wait()
            if not self.checked_cnt == self.total_cnt:
                self.done.wait()

            self.result_collector_lock.acquire()
            self.result_collector_lock.release()
        finally:
            self.report_fd.close()
            if not self.quiet:
                if self.print_status_t.is_alive():
                    self.print_status_t.cancel()
                    self.get_status()
            s =    "Files scanned:            %d" % self.checked_cnt
            s += "\n - Duplicate files found: %d" % self.dup_cnt
            s += "\n - Max threads:           %d" % self.max_threads
            print(s)

if __name__ == '__main__':
    try:
        start = time.time()
        DuplicateFinder(targets = args.targets,
                        max_threads = max_threads,
                        output_file = args.output_file,
                        remove_duplicates = args.remove_duplicates,
                        quiet = args.quiet)()
        print("Time taken: %.2f" % (time.time() - start))
    except:
        raise
