Script to identify duplicate media (photo, video and audio files) in a fast way

```
./duplicate_media_finder.py -h

usage: duplicate_media_finder.py [-h] [--single-threaded]
                                 [--output-file OUTPUT_FILE]
                                 [--remove-duplicates] [--quiet]
                                 targets [targets ...]

positional arguments:
  targets

optional arguments:
  -h, --help            show this help message and exit
  --single-threaded, -s
                        Run single threaded mode
  --output-file OUTPUT_FILE, -o OUTPUT_FILE
                        Output file. Defaults to duplicates.log
  --remove-duplicates, -r
                        Remove duplicates
  --quiet, -q           Don't print progress

```

Result examples:

```
                  |       Database info       | Processing time
                  |  # of  |         |        | multi  | single
  Processor info  | files  |  Size   | % dupl | thread | thread 
----------------------------------------------------------------
Intel Core2 Duo   |   13k  |  34 GB  |    1%  |  0.4   | 0.55
Intel Core2 Duo   |   26k  |  68 GB  |   50%  |   32   |   46
Intel Core2 Duo   |   32k  |  85 GB  |   47%  |   70   |   93
Intel Core2 Duo   |   95k  | 348 GB  |   64%  |  580   |  895
Pentium Dual-Core |   48k  | 151 GB  |   66%  |  160   |  210
Inter Core i5        |   17k  | 42 GB  |   3%  |  1.23   |  2.57
```