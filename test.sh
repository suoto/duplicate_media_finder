#!/bin/bash

DB_REF='/media/souto/DRIVE_D/temp/Fotos'
DB_DUP='/media/souto/DRIVE_D/temp/ref_dup/'

EXTRA='-q'
#EXTRA=''

LOG_FILE="test.log"

printf "" > $LOG_FILE

function single_run() {
	if [ "$1" == "nd" ]; then
		TARGET=" $DB_REF"
	elif [ "$1" == "fd" ]; then
		TARGET=" $DB_REF  $DB_DUP"
	elif [ "$1" == "all" ]; then
		TARGET=" /media/souto/DRIVE_D  /home/souto/*"
	else
		echo "Option $1 is invalid. Valid options are -nd (no dup), -fd (full dup) or -pd (partial dup)"
		exit
	fi
	
	for e in '' --single-threaded; do
		CMD="./duplicate_media_finder.py ${TARGET} ${e} $EXTRA"
		echo $CMD
		echo "./duplicate_media_finder.py ${TARGET} ${e} $EXTRA" >> $LOG_FILE
		$CMD >> $LOG_FILE
		sleep 1
		echo "----------------------------------" >> $LOG_FILE
		$CMD >> $LOG_FILE
		sleep 1
		echo "=================================================================================" >> $LOG_FILE
	done
}


for action in all nd fd; do
	single_run "${action}"
	echo "=================================================================================" >> $LOG_FILE
	echo "=================================================================================" >> $LOG_FILE
	echo "=================================================================================" >> $LOG_FILE
done;
